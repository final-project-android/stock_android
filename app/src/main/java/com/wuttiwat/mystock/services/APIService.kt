package com.wuttiwat.mystock.services

import com.wuttiwat.mystock.API_PRODUCT
import com.wuttiwat.mystock.API_PRODUCT_PARAMS_ID
import com.wuttiwat.mystock.model.ProductRequest
import com.wuttiwat.mystock.model.ProductResponseItem
import retrofit2.Call
import retrofit2.http.*


interface APIService {
    @GET(API_PRODUCT)
    fun getProduct(): Call<List<ProductResponseItem>>

    @POST(API_PRODUCT)
    fun addProduct(@Body product: ProductRequest): Call<Any>

    @PUT("$API_PRODUCT/{$API_PRODUCT_PARAMS_ID}")
    fun editProduct(@Path(API_PRODUCT_PARAMS_ID) id: Int, @Body product: ProductRequest): Call<Any>

    @DELETE("$API_PRODUCT/{$API_PRODUCT_PARAMS_ID}")
    fun deleteProduct(@Path(API_PRODUCT_PARAMS_ID) id: Int): Call<Any>
}