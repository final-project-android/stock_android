package com.wuttiwat.mystock

const val PREFS_KEY_USERNAME = "username"
const val PREFS_KEY_IS_LOGIN = "is_login"

const val BASE_URL = "http://192.168.1.121:3000/"
const val IMAGE_URL = "images"
const val API_PRODUCT = "product"
const val API_PRODUCT_PARAMS_ID = "id"
