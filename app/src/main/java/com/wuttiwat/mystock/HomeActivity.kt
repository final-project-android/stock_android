package com.wuttiwat.mystock

import android.content.Intent
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.wuttiwat.mystock.adapter.SectionsPagerAdapter
import com.wuttiwat.mystock.databinding.ActivityHomeBinding
import com.wuttiwat.mystock.databinding.CustomTabMenuBinding

class HomeActivity : AppCompatActivity() {

    private lateinit var sectionsPagerAdapter: SectionsPagerAdapter
    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager, lifecycle)

        setUpViewPage()
        setUpWidget()
        setUpTab()

    }

    private fun setUpTab() {
        TabLayoutMediator(binding.tabs, binding.viewPager,
            TabLayoutMediator.TabConfigurationStrategy{
                tab, position ->
                val binding = CustomTabMenuBinding.inflate(layoutInflater)
                binding.iconTab.setImageResource(sectionsPagerAdapter.tabIcon[position])
                binding.textTab.text = sectionsPagerAdapter.tabText[position]
                tab.customView = binding.root
            }).attach()
    }

    private fun setUpWidget() {
        binding.fab.setOnClickListener { view ->
            Intent(applicationContext, CreateActivity::class.java).run {
                startActivity(this)
            }
        }
    }

    private fun setUpViewPage() {
        binding.viewPager.apply {
            adapter = sectionsPagerAdapter
        }.also {
            it.setPageTransformer(HorizontalFlipTransformation())
            it.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback(){
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    if(position == 0){
                        binding.fab.visibility = View.INVISIBLE
                    }
                    else{
                        binding.fab.visibility = View.VISIBLE
                    }
                }
            })
        }
    }
}