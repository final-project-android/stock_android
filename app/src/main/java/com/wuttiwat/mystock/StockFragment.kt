package com.wuttiwat.mystock

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.wuttiwat.mystock.adapter.ProductListAdapter
import com.wuttiwat.mystock.adapter.StockListAdapter
import com.wuttiwat.mystock.databinding.FragmentStockBinding
import com.wuttiwat.mystock.model.ProductResponseItem
import com.wuttiwat.mystock.services.APIClient
import com.wuttiwat.mystock.services.APIService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class StockFragment : Fragment() {

    private lateinit var binding: FragmentStockBinding
    private lateinit var customAdapter: StockListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStockBinding.inflate(layoutInflater)
        customAdapter = StockListAdapter(null)

        setupWidget()
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        feedNetwork()
    }

    private fun setupWidget() {
        binding.stockRecycleview.apply {
            adapter = customAdapter
            layoutManager = LinearLayoutManager(context)
        }.also {
            it.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            it.setHasFixedSize(true)
        }
        binding.swipeRefresh.setOnRefreshListener {
            feedNetwork()
        }
    }

    private fun feedNetwork() {
        binding.swipeRefresh.isRefreshing = true
        APIClient.getClient().create(APIService::class.java).getProduct().let { call ->
            call.enqueue(object : Callback<List<ProductResponseItem>> {
                override fun onResponse(
                    call: Call<List<ProductResponseItem>>,
                    response: Response<List<ProductResponseItem>>
                ) {
                    if (response.isSuccessful) {
                        binding.stockRecycleview.adapter = StockListAdapter(response.body())
                    } else {
                        context?.showToast(response.message())
                    }
                    binding.swipeRefresh.isRefreshing = false
                }

                override fun onFailure(call: Call<List<ProductResponseItem>>, t: Throwable) {
                    context?.showToast(t.message.toString())
                    binding.swipeRefresh.isRefreshing = false
                }

            })
        }

    }

}