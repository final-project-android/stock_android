package com.wuttiwat.mystock

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.wuttiwat.mystock.databinding.ActivityCreateBinding
import com.wuttiwat.mystock.model.ProductRequest
import com.wuttiwat.mystock.services.APIClient
import com.wuttiwat.mystock.services.APIService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class CreateActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCreateBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCreateBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(Color.parseColor("#2cb46b")))

        setupWidget()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)



    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if(item.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupWidget() {
        binding.buttonSubmit.setOnClickListener {
            val name = binding.edtName.text.toString()
            val price = binding.edtPrice.text.toString()
            val stock = binding.edtStock.text.toString()

            val textToNumber: (String) -> Int = { text ->
                if (text.isEmpty()) {
                    0
                } else {
                    text.toInt()
                }
            }

            ProductRequest(name, textToNumber(price), textToNumber(stock)).run {
                addProduct(this)
            }

        }
    }


    private fun addProduct(product: ProductRequest) {
        APIClient.getClient().create(APIService::class.java).addProduct(product).let { call ->
            call.enqueue(object : Callback<Any> {
                override fun onResponse(
                    call: Call<Any>,
                    response: Response<Any>
                ) {
                    if (response.isSuccessful) {
                        finish()

                    } else {
                        showToast("Add Product Failed")
                    }
                }

                override fun onFailure(call: Call<Any>, t: Throwable) {
                    showToast(t.message.toString())
                }

            })
        }

    }
}