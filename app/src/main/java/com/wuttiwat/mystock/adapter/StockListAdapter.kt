package com.wuttiwat.mystock.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.wuttiwat.mystock.R
import com.wuttiwat.mystock.databinding.ProductListBinding
import com.wuttiwat.mystock.databinding.StockListBinding
import com.wuttiwat.mystock.model.ProductResponseItem
import com.wuttiwat.mystock.services.APIClient

class StockListAdapter(var stockList: List<ProductResponseItem>?) : RecyclerView.Adapter<StockListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            StockListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding.root, binding)
    }

    override fun getItemCount() = stockList?.size ?: 0

    override fun onBindViewHolder(holder: StockListAdapter.ViewHolder, position: Int) {
        val binding = holder.binding


        with(binding){
            stockList?.let {
                    list ->
                val item = list[position]
                textviewName.text = item.name
                textviewDetail.text = ""
                textviewPrice.text = "฿ ${item.price}"
                textviewStock.text = "${item.stock} price"

                Glide
                    .with(imageviewProduct.context)
                    .load(APIClient.getImageURL() + item.image)
                    .error(R.drawable.meb_logo)
                    .into(imageviewProduct)
            }


        }

    }

    inner class ViewHolder(view: View, val binding: StockListBinding) :
        RecyclerView.ViewHolder(view) {

    }
}