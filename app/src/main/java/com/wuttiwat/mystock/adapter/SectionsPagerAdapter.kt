package com.wuttiwat.mystock.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStateManagerControl
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.wuttiwat.mystock.ProductFragment
import com.wuttiwat.mystock.R
import com.wuttiwat.mystock.StockFragment


class SectionsPagerAdapter(private val context: Context, fm: FragmentManager, lifecycle: Lifecycle)
    : FragmentStateAdapter(fm, lifecycle){

    val tabIcon: Array<Int> = arrayOf(R.drawable.ic_product,R.drawable.ic_stock)
    var tabText: Array<String> = arrayOf(*context.resources.getStringArray(R.array.tab_title))

    override fun getItemCount() = tabIcon.size

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0-> {
                ProductFragment()
            }
            else -> {
                StockFragment()
            }
        }
    }
}