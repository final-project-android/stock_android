package com.wuttiwat.mystock

import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.wuttiwat.mystock.adapter.ProductListAdapter
import com.wuttiwat.mystock.databinding.FragmentProductBinding
import com.wuttiwat.mystock.model.ProductResponseItem
import com.wuttiwat.mystock.services.APIClient
import com.wuttiwat.mystock.services.APIService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ProductFragment : Fragment() {


    private lateinit var binding: FragmentProductBinding
    private lateinit var customAdapter: ProductListAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProductBinding.inflate(layoutInflater)
        customAdapter = ProductListAdapter(null)

        setupWidget()
        feedNetwork()


        return binding.root
    }

    private fun feedNetwork() {
        binding.swipeRefresh.isRefreshing = true
        APIClient.getClient().create(APIService::class.java).getProduct().let { call ->
            call.enqueue(object : Callback<List<ProductResponseItem>> {
                override fun onResponse(
                    call: Call<List<ProductResponseItem>>,
                    response: Response<List<ProductResponseItem>>
                ) {
                    if (response.isSuccessful) {
                        binding.productRecycleview.adapter = ProductListAdapter(response.body())
                    } else {
                        context?.showToast(response.message())
                    }
                    binding.swipeRefresh.isRefreshing = false
                }

                override fun onFailure(call: Call<List<ProductResponseItem>>, t: Throwable) {
                    context?.showToast(t.message.toString())
                    binding.swipeRefresh.isRefreshing = false
                }

            })
        }

    }

    private fun setupWidget() {
        binding.productRecycleview.apply {
            adapter = customAdapter
            layoutManager = GridLayoutManager(context, 2)
        }.also {
            it.addItemDecoration(GridSpacingItemDecoration(2, 30, true))
            it.setHasFixedSize(true)
        }
        binding.swipeRefresh.setOnRefreshListener {
            feedNetwork()
        }
    }

}